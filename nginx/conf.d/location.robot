location ~* robots.txt {
    add_header Cache-Control public;
        add_header ETag "";
    expires $expires;
    return 200 
'User-agent: *
Disallow: /
Allow: *.jpg
Allow: *.jpeg
Allow: *.png
Allow: *.gif
Allow: *.css
Allow: *.svg
Allow: *.js'
    ;
}
