#!/bin/bash
docker run -it --rm -v /docker-compose/nginx/ssl.d/letsencrypt/:/etc/letsencrypt -v /docker-compose/nginx/ssl.d/letsencrypt/cloudflare.ini:/cloudflare.ini -v /var/log/letsencrypt:/var/log/letsencrypt certbot/dns-cloudflare  certonly --dns-cloudflare --dns-cloudflare-credentials="/cloudflare.ini" -d *.novoe.od.ua
